BRANCH=18.08
ARCH=$(shell uname -m | sed "s/^i.86$$/i686/")
ifeq ($(ARCH),i686)
FLATPAK_ARCH=i386
else
FLATPAK_ARCH=$(ARCH)
endif
REPO=./repo
GPG_OPTS="--gpg-sign=A36748ACD4E9CCAAE460B9D94FF2B68C56CF6A95"
ARCH_OPTS=-o target_arch $(ARCH)

RUNTIMES=				\
	sdk				\
	sdk-debug			\
	sdk-docs			\
	sdk-locale			\
	platform			\
	platform-locale			\
	platform-arch-libs		\
	platform-arch-libs-debug

RUNTIME_ELEMENTS=$(addprefix flatpak/,$(addsuffix .bst,$(RUNTIMES)))

WINE_RUNTIMES=      \
    platform-wine-3.21-stage

#    platform-wine-3.8-stage \
#    platform-wine-2.21-stage \
#    platform-wine-3.20-stage

WINE_RUNTIME_ELEMENTS=$(addprefix flatpak/,$(addsuffix .bst,$(WINE_RUNTIMES)))


CHECKOUT_ROOT=runtimes

all: build

build:
	export LC_ALL=C
	bst --colors $(ARCH_OPTS) --verbose build   all.bst

#evaluate  --track-all --track-except

export:
	export LC_ALL=C
	bst --colors $(ARCH_OPTS) build $(RUNTIME_ELEMENTS)
	
	mkdir -p $(CHECKOUT_ROOT)
	set -e; for runtime in $(RUNTIMES); do \
	  dir="$(ARCH)-$${runtime}"; \
	  bst --colors $(ARCH_OPTS) checkout --hardlinks "flatpak/$${runtime}.bst" "$(CHECKOUT_ROOT)/$${dir}"; \
	  flatpak build-export --arch=$(FLATPAK_ARCH) --files=files $(GPG_OPTS) $(REPO) "$(CHECKOUT_ROOT)/$${dir}" "$(BRANCH)"; \
	  rm -rf "$(CHECKOUT_ROOT)/$${dir}"; \
	done

	set -e; case "$(RUNTIMES)" in \
	  *platform-arch-libs*) \
	    if test "$(ARCH)" = "i686" ; then \
	      flatpak build-commit-from $(GPG_OPTS) --src-ref=runtime/online.winehub.Platform.Compat.$(FLATPAK_ARCH)/$(FLATPAK_ARCH)/$(BRANCH) $(REPO) runtime/online.winehub.Platform.Compat.$(FLATPAK_ARCH)/x86_64/$(BRANCH); \
	      flatpak build-commit-from $(GPG_OPTS) --src-ref=runtime/online.winehub.Platform.Compat.$(FLATPAK_ARCH).Debug/$(FLATPAK_ARCH)/$(BRANCH) $(REPO) runtime/online.winehub.Platform.Compat.$(FLATPAK_ARCH).Debug/x86_64/$(BRANCH); \
	    fi \
	esac

build-wine:
	bst --colors $(ARCH_OPTS) --verbose build  flatpak/platform-wine-3.21-stage.bst
#	bst --colors $(ARCH_OPTS) --verbose build  flatpak/platform-wine-3.20-stage.bst
#	bst --colors $(ARCH_OPTS) --verbose build  flatpak/platform-wine-2.21-stage.bst
#	bst --colors $(ARCH_OPTS) --verbose build  flatpak/platform-wine-3.8-stage.bst


export-wine:
	export LC_ALL=C
	bst --colors $(ARCH_OPTS) build $(WINE_RUNTIME_ELEMENTS)

	mkdir -p $(CHECKOUT_ROOT)
	set -e; for runtime in $(WINE_RUNTIMES); do \
	  dir="$(ARCH)-$${runtime}"; \
	  bst --colors $(ARCH_OPTS) checkout --hardlinks "flatpak/$${runtime}.bst" "$(CHECKOUT_ROOT)/$${dir}"; \
	  flatpak build-export --arch=x86_64 --files=files $(GPG_OPTS) $(REPO) "$(CHECKOUT_ROOT)/$${dir}" "$(BRANCH)"; \
	  rm -rf "$(CHECKOUT_ROOT)/$${dir}"; \
	done

test-wine-shell:
	flatpak remote-add --if-not-exists --user --no-gpg-verify wine-sdk-test-repo $(REPO)
	flatpak install -y --arch=$(FLATPAK_ARCH) --user wine-sdk-test-repo online.winehub.{Platform,Platform.Compat.i386,Sdk}//$(BRANCH)
	flatpak install -y --user wine-sdk-test-repo runtime/online.winehub.Platform.Wine32/i386/3.21-stage
	flatpak-builder --arch=$(FLATPAK_ARCH) --force-clean app tests/online.winehub.shell.json
	flatpak-builder --arch=$(FLATPAK_ARCH) --run app tests/online.winehub.shell.json shell-test

	flatpak remove -y runtime/online.winehub.Platform.Wine32/i386/3.21-stage
	flatpak remove -y --arch=$(FLATPAK_ARCH)  online.winehub.{Platform,Sdk,Sdk.Docs}/i386/$(BRANCH)
	flatpak remove -y --arch=$(FLATPAK_ARCH)  online.winehub.{Platform,Platform.Compat.i386,Sdk,Sdk.Docs}//$(BRANCH)
	flatpak remote-delete wine-sdk-test-repo

wow: $(REPO)
	flatpak-builder --arch=$(FLATPAK_ARCH) --force-clean app tests/com.blizzard.Wow.yml
	flatpak-builder --arch=$(FLATPAK_ARCH) --run app tests/com.blizzard.Wow.yml wow

diablo: $(REPO)
	flatpak-builder --arch=$(FLATPAK_ARCH) --force-clean app tests/com.blizzard.DiabloIII.yml
	flatpak-builder --arch=$(FLATPAK_ARCH) --run app tests/com.blizzard.DiabloIII.yml diablo

swtor: $(REPO)
	flatpak-builder --arch=$(FLATPAK_ARCH) --force-clean app tests/com.BioWare.SWTOR.yml
	flatpak-builder --arch=$(FLATPAK_ARCH) --run app tests/com.BioWare.SWTOR.yml swtor

clean:
	rm -rf $(CHECKOUT_ROOT)
	rm -rf $(REPO)

clean-runtime:
	rm -rf $(CHECKOUT_ROOT)
